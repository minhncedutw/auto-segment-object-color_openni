# Auto-segment-object-color #

### What is this repository for? ###

* Quick summary: Segment the object according to their colors using simple image processing. Using OpenNI library to record scene.
* Version: 1.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up:

1. Clone repository: `git clone https://minhncedutw@bitbucket.org/minhncedutw/auto-segment-object-color_openni.git`

2. Install driver like guide at: `/driver/Install_driver.md`

3. Create then Activate environment: 
```
conda env create -f environment.yml # create environment
source activate pointnet_pytorch # activate installed environment
```

4. Run tests

* Configuration
* Dependencies
* Database configuration
* How to run tests
4. Run finding-color program to find good HSV-value-range to segment object out of scene:
`python find_color_range`
5. After finding good HSV-value-range, execute recording and segmenting object out of scene:
`python record_data`
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact